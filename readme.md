#JBA Developer Test

##Installation instructions

1. Clone this repo
```bash
git clone https://lucentx@bitbucket.org/lucentx/jba-developer-test.git
```

2. Start all docker containers by running
```bash
docker-compose up -d
```

3. Create a laravel `.env` file by copying the .env.example
```bash
cp ./src/env.example ./src/.env
```

4. Edit `/src/.env` file to use database config set on `docker-compose.yml` file e.g.
```
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=homestead
DB_PASSWORD=secret
```

5. Run composer install
```bash
docker-compose exec app composer install
```

6. Generate laravel application key by running
```bash
docker-compose exec app php artisan key:generate
```

6. Cache laravel config files
```bash
docker-compose exec app php artisan config:cache
```

7. Run migrations
```bash
docker-compose exec app php artisan migrate
```

8. Run database seeders
```bash
docker-compose exec app php artisan db:seed
```

9. Generate passport personal access tokens
```bash
docker-compose exec app php artisan passport:client --personal
```

10. Build angular frontend
```bash
docker-compose exec node ng build --aot
```

11. Run `docker-machine ip` and add the ip to your hosts file
```
192.168.99.100 api.jb-developer.test
192.168.99.100 jb-developer.test
```

12. Visit `http://jb-developer.test` and login with the seeded credentials


User credentials seeded

Admin user:

username: `barbosa.aron@mail.com`
password: `password`

Normak user:

username: `barbosa.aron+nomal-user@gmail.com`
password: `password`