import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, ProductsService } from '../_services';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {
  selectedFile: any;
  productAddForm: FormGroup;
  loading = false;
  submitted = false;
  hasFile = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductsService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.productAddForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required],
      image: ['', Validators.required]
    });
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.hasFile = true;
  }

  // convenience getter for easy access to form fields
  get f() { return this.productAddForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.productAddForm.invalid) {
      return;
    }

    this.loading = true;
    this.productService.addProduct(this.f.title.value, this.f.description.value, this.f.price.value, this.selectedFile)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.alertService.success(data.message);
          this.productAddForm.reset();
          Object.keys(this.productAddForm.controls).forEach(key => {
            this.productAddForm.controls[key].setErrors(null)
          });
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

}
