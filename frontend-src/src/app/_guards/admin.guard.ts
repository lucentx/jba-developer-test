import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  currentUser: any;
  adminRoles: any;

  constructor(private router: Router) {
    this.adminRoles = ['admin'];
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    let isAdmin = false;

    this.currentUser.roles.forEach((value) => {
      if (this.adminRoles.indexOf(value) !== -1) {
        isAdmin = true;
      }
    });

    if (!isAdmin) {
      this.router.navigate(['/not-authorized']);
      return false;
    }

    return true;
  }
  
}
