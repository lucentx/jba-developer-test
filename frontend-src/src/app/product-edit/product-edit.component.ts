import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { AlertService, ProductsService } from '../_services';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {
  selectedFile: any;
  productEditForm: FormGroup;
  productId: any;
  loading = false;
  submitted = false;
  hasFile = false;
  siteUrl: any;
  product: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductsService,
    private alertService: AlertService
  ) {
    this.siteUrl = environment.siteUrl;
  }

  ngOnInit() {
    this.productEditForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required],
      image_update: ['']
    });

    this.route.paramMap.subscribe(params => {
      this.productId = params.get("id")
    });

    this.fetchProduct();
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.hasFile = true;
  }

  // convenience getter for easy access to form fields
  get f() { return this.productEditForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.productEditForm.invalid) {
      return;
    }

    this.loading = true;
    this.productService.updateProduct(this.productId, this.f.title.value, this.f.description.value, this.f.price.value, this.selectedFile)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.alertService.success(data.message);
          this.productEditForm.patchValue({
            image_update: ''
          });
          this.fetchProduct();
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  fetchProduct() {
    this.productService.getProduct(this.productId)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.product = data;
          if (!this.product) {
            this.alertService.error('Product not found.');
          }
          this.productEditForm.patchValue({
            title: this.product.title,
            description: this.product.description,
            price: this.product.price
          });
        }
      );
  }

}
