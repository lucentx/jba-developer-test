import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ImageCompressService, AlertService, AuthenticateService } from '../_services';

@Component({
  selector: 'app-image-compress',
  templateUrl: './image-compress.component.html',
  styleUrls: ['./image-compress.component.scss']
})
export class ImageCompressComponent implements OnInit {
  selectedFile = null;
  loading = false;
  submitted = false;
  downloadUrl = null;
  
  constructor(
    private router: Router,
    private imageCompressService: ImageCompressService,
    private alertService: AlertService
  ) { }

  ngOnInit() {

  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  onUpload() {
    this.submitted = true;
    this.loading = true;
    this.imageCompressService.compress(this.selectedFile)
      .pipe(first())
      .subscribe(
          (data: any) => {
            this.alertService.success('Image compression. Click download button to download the compressed image.');
            if (data.hasOwnProperty('file_download')) {
              this.downloadUrl = data.file_download;
            }
            this.loading = false;
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          });
  }
}
