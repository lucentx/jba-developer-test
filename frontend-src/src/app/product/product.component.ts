import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first, map } from 'rxjs/operators';
import { ProductsService , AlertService, AuthenticateService } from '../_services';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  productId: any;
  siteUrl: any;
  product: any;
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductsService,
    private alertService: AlertService
  ) {
  }

  ngOnInit() {
    this.siteUrl = environment.siteUrl;

    this.route.paramMap.subscribe(params => {
      this.productId = params.get("id")
    });

    this.productService.getProduct(this.productId)
      .pipe(first())
      .subscribe(
        data => {
          this.product = data;
        }
      );
  }

}
