import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'header-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements OnInit {
  currentUser: any;
  adminRoles: any;
  isLoggedIn = false;
  isAdmin = false;

  constructor() {

  }

  ngOnInit() {
    this.setMenu();

    setInterval(() => this.setMenu(), 100);
  }

  setMenu() {
    this.adminRoles = ['admin'];
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    if (this.currentUser) {
      if (this.currentUser.token) {
        this.isLoggedIn = true;
      }
  
      let isAdmin = false;
  
      this.currentUser.roles.forEach((value) => {
        if (this.adminRoles.indexOf(value) !== -1) {
          isAdmin = true;
        }
      });
  
      this.isAdmin = isAdmin;
    } else {
      this.isLoggedIn = false;
      this.isAdmin = false;
    }
  }



}