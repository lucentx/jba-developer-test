import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { LogoutComponent } from './logout';
import { ImageCompressComponent } from './image-compress';
import { ProductsComponent } from './products';
import { ProductComponent } from './product';
import { ProductEditComponent } from './product-edit';
import { ProductAddComponent } from './product-add';
import { NotAuthorizedComponent } from './not-authorized';
import { InventoryComponent } from './inventory';
import { AuthGuard } from './_guards';
import { AdminGuard } from './_guards';

const appRoutes: Routes = [
    { path: '', component: ProductsComponent, canActivate: [AuthGuard] },
    { path: 'image-compress', component: ImageCompressComponent, canActivate: [AuthGuard] },
    { path: 'product/add', component: ProductAddComponent, canActivate: [AuthGuard, AdminGuard] },
    { path: 'product/:id', component: ProductComponent, canActivate: [AuthGuard] },
    { path: 'product/:id/edit', component: ProductEditComponent, canActivate: [AuthGuard, AdminGuard] },
    { path: 'inventory', component: InventoryComponent, canActivate: [AuthGuard] },
    { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'not-authorized', component: NotAuthorizedComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);