import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService, AuthenticateService } from '../_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(
    private authenticateService: AuthenticateService,
    private alertService: AlertService,
    private router: Router
  ) { }

  ngOnInit() {
    this.authenticateService.logout()
    .pipe(first())
    .subscribe(
        data => {
          localStorage.removeItem('currentUser');
          this.router.navigate(['/login']);
        },
        error => {
          this.alertService.error(error);
        });;
  }

}
