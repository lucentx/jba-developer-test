import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImageCompressService {
  currentUser: any;
  options: any;
  token: '';
  constructor(private http: HttpClient) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = (this.currentUser && this.currentUser.hasOwnProperty('token')) ? this.currentUser.token : '';

    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token
    });
    let options = { headers: headers };

    this.options = options;
  }

  compress(file: File) {
    const uploadData = new FormData();
    uploadData.append('image_file', file, file.name);

    return this.http.post<any>(`${environment.apiUrl}/image-compress`, uploadData, this.options)
      .pipe(map(data => {
        return data;
      }),
      catchError(err => {
        return err;
      }));
  }
}
