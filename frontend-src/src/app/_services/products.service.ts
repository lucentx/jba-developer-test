import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  currentUser: any;
  options: any;
  token = '';
  
  constructor(private http: HttpClient) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = (this.currentUser && this.currentUser.hasOwnProperty('token')) ? this.currentUser.token : '';

    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token
    });
    let options = { headers: headers };

    this.options = options;
  }

  getAll() {
    return this.http.get<any>(`${environment.apiUrl}/products`, this.options)
      .pipe(map(data => {
        return data;
      }),
      catchError(err => {
        return err;
      }));
  }

  getProduct(id:number) {
    return this.http.get<any>(`${environment.apiUrl}/products/${id}`, this.options)
      .pipe(map(data => {
        return data;
      }),
      catchError(err => {
        return err;
      }));
  }

  addProduct(title:string, description:string, price:string, file:File) {
    const postData = new FormData();
    postData.append('title', title);
    postData.append('description', description);
    postData.append('price', price);
    postData.append('image_file', file, file.name);

    return this.http.post<any>(`${environment.apiUrl}/products`, postData, this.options)
      .pipe(map(data => {
        return data;
      }),
      catchError(err => {
        return err;
      }));
  }

  updateProduct(id: string, title:string, description:string, price:string, file?:File) {
    const postData = new FormData();
    postData.append('title', title);
    postData.append('description', description);
    postData.append('price', price);
    if (file) {
      postData.append('image_file', file, file.name);
    }

    return this.http.post<any>(`${environment.apiUrl}/products/${id}`, postData, this.options)
      .pipe(map(data => {
        return data;
      }),
      catchError(err => {
        return err;
      }));
  }

  addToInventory(id: any) {
    return this.http.post<any>(`${environment.apiUrl}/inventory/add`, { product_id : id }, this.options)
      .pipe(map(data => {
        return data;
      }),
      catchError(err => {
        return err;
      }));
  }

  getInventory() {
    return this.http.get<any>(`${environment.apiUrl}/inventory`, this.options)
      .pipe(map(data => {
        return data;
      }),
      catchError(err => {
        return err;
      }));
  }
}
