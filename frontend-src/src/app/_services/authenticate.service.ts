import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {
  currentUser: any;
  options: any;
  token: '';

  constructor(private http: HttpClient) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = (this.currentUser && this.currentUser.hasOwnProperty('token')) ? this.currentUser.token : '';

    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token
    });
    let options = { headers: headers };

    this.options = options;
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/login`, { email: username, password: password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      }));
  }

  logout() {
    return this.http.get<any>(`${environment.apiUrl}/logout`, this.options)
      .pipe(map(data => {
        return data;
      }),
      catchError(err => {
        return err;
      }));
  }
}
