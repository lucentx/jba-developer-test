import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ProductsService , AlertService, AuthenticateService } from '../_services';
import { environment } from './../../environments/environment';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  products: {};
  siteUrl: string;
  currentUser: any;
  adminRoles: any;
  isLoggedIn = false;
  isAdmin = false;

  constructor(
    private router: Router,
    private productService: ProductsService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.siteUrl = environment.siteUrl;

    this.productService.getInventory()
      .pipe(first())
      .subscribe(
        data => {
          this.products = data;
        }
      );
  }

}
