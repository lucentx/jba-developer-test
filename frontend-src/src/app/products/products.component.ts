import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ProductsService , AlertService, AuthenticateService } from '../_services';
import { environment } from './../../environments/environment';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: {};
  siteUrl: string;
  currentUser: any;
  adminRoles: any;
  isLoggedIn = false;
  isAdmin = false;

  constructor(
    private router: Router,
    private productService: ProductsService,
    private alertService: AlertService
  ) {
    this.adminRoles = ['admin'];
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let isAdmin = false;

    this.currentUser.roles.forEach((value) => {
      if (this.adminRoles.indexOf(value) !== -1) {
        isAdmin = true;
      }
    });

    this.isAdmin = isAdmin;
  }

  ngOnInit() {
    this.siteUrl = environment.siteUrl;

    this.productService.getAll()
      .pipe(first())
      .subscribe(
        data => {
          this.products = data;
        }
      );
  }

  addToInventory(product) {
    this.productService.addToInventory(product.id)
    .pipe(first())
    .subscribe(
      (data: any) => {
        this.alertService.success(data.message);
      }
    );
  }

}
