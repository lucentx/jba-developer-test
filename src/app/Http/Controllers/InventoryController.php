<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use Auth;
class InventoryController extends Controller
{

  public function index() {
    try {
      $user = Auth::user();
      return response($user->inventory()->get());
    } catch (\Exception $e) {
      return response(['message' => $e->getMessage()], 422);
    }
  }

  /**
   * Add product to inventory
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function add_product_to_inventory(Request $request)
  {
    try {
      $product = Product::find($request->input('product_id'));
      $user = Auth::user();
      $user->inventory()->attach($product->id);
      return response(['message' => 'Product ID#' . $product->id . ' added to inventory']);
    } catch (\Exception $e) {
      return response(['message' => 'Something went wrong.'], 422);
    }
    
  }
}
