<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;
use App\User;
use Hash;
use Auth;

class AuthenticationController extends Controller
{
    public function login(Request $request) {
        $user = User::where('email', $request->email)->first();
        
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token, 'roles' => $user->roles->pluck(['slug'])];
                return response($response, 200);
            } else {
                $response = 'Password mismatch';
                return response(['message' => $response], 422);
            }
        } else {
            $response = 'User doesn\'t exist';
            return response(['message' => $response], 422);
        }
    }
    public function logout(Request $request) {
        if (Auth::check()) {
            Auth::user()->AauthAcessToken()->delete();

            $response = 'You have been successfully logged out!';
            return response(['message' => $response], 200);
        }
    }
}
