<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ImageOptimizer;
use File;
use Zip;
use Hash;

class ImageOptimizationController extends Controller
{
  public function imageCompressService(Request $request) {
    try {
      $image = $request->file('image_file');

      $name = time() . '-original.' . \File::extension($image->getClientOriginalName());
      $compressed_name = time() . '-compressed.' . \File::extension($image->getClientOriginalName());

      $upload_folder = storage_path() . '/app/public/uploads/';
      $compressed_folder = storage_path() . '/app/public/compressed/';
      
      if (!File::exists($compressed_folder)) {
        File::makeDirectory($compressed_folder);
      }

      $file_path = $upload_folder . $name;
      $file_compressed_path = $compressed_folder . $compressed_name;

      $request->file('image_file')->move($upload_folder, $name);

      // Optimize image
      ImageOptimizer::optimize($file_path, $file_compressed_path);
      
      // Zip original image and compressed image
      $zip_name = time() . '-compressed.zip';
      if (!File::exists(public_path() . '/downloads/')) {
        File::makeDirectory(public_path() . '/downloads/');
      }

      $zip = Zip::create(public_path() . '/downloads/' . $zip_name);
      $zip->add($file_path)->add($file_compressed_path);
      $zip->close();

      return response(['file_download' => url('/downloads/' . $zip_name)]);
    } catch (Exception $e) {

    }

  }
}
