<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Product;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return response($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'image_file' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['message' => 'Validation error.'], 400);
        }

        try {
            if ($request->hasFile('image_file')) {
                $image = $request->file('image_file');
                $image_name = time() . '-' . $image->getClientOriginalName();
                $request->file('image_file')->move(public_path() . '/uploads/products', $image_name);
            }
    
            $product = new Product;
            $product->title = $request->input('title');
            $product->description = $request->input('description');
            $product->price = $request->input('price');
            $product->image_url = '/uploads/products/' . $image_name;
            $product->save();

            return response(['message' => 'Added new product.']);
        } catch (\Exception $e) {
            return response(['message' => 'Something went wrong. Refresh the page and try again.'], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return response($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['message' => 'Validation error.'], 400);
        }

        try {
            if ($request->hasFile('image_file')) {
                $image = $request->file('image_file');
                $image_name = time() . '-' . $image->getClientOriginalName();
                $request->file('image_file')->move(public_path() . '/uploads/products', $image_name);
            }
    
            $product = Product::find($id);
            $product->title = $request->input('title');
            $product->description = $request->input('description');
            $product->price = $request->input('price');
            if ($request->hasFile('image_file')) {
                $product->image_url = '/uploads/products/' . $image_name;
            }
            $product->save();

            return response(['message' => 'Product updated.']);
        } catch (\Exception $e) {
            return response(['message' => 'Something went wrong. Refresh the page and try again.'], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
