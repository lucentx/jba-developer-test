<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->insert([
            [
                'title' => 'Bag',
                'description' => 'This is a sample bag.',
                'price' => 99.9,
                'image_url' => '/uploads/backpack.jpg'
            ],
            [
                'title' => 'Skateboard',
                'description' => 'This is a sample skateboard.',
                'price' => 105.45,
                'image_url' => '/uploads/skateboard.jpg'
            ],
            [
                'title' => 'Shoes',
                'description' => 'This is a sample shoes.',
                'price' => 50.00,
                'image_url' => '/uploads/shoes.jpg'
            ]
        ]);
    }
}
