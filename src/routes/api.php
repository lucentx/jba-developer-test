<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Public Route
Route::post('/login','Auth\AuthenticationController@login')->name('login');

// Private Route
Route::middleware('auth:api')->group(function () {
    Route::get('/logout','Auth\AuthenticationController@logout')->name('logout');

    // Image compression
    Route::post('/image-compress', 'ImageOptimizationController@imageCompressService')->name('image-compress');

    // Products
    Route::get('/products', 'ProductsController@index')->name('products.all');
    Route::get('/products/{id}', 'ProductsController@show')->name('products.show');
    Route::post('/products', 'ProductsController@store')->name('products.store');
    Route::post('/products/{id}', 'ProductsController@update')->name('products.update');

    // Inventory
    Route::get('/inventory', 'InventoryController@index')->name('inventory.all');
    Route::post('/inventory/add', 'InventoryController@add_product_to_inventory')->name('inventory.add-product');
});